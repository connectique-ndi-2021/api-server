﻿using connectique_bo.DTOs.Auth;
using connectique_bo.DTOs.Identity;
using connectique_bo.Entities.Identity;
using contectique_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Security.Authentication;

namespace contectique_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        AuthService authService;

        public AuthController(AuthService authService)
        {
            this.authService = authService;

        }


        [AllowAnonymous]
        [HttpPost("Login")]
        public ActionResult<string> Login(AuthenticationDTO authenticationDTO)
        {
            try
            {
                return authService.Login(authenticationDTO);
            }
            catch (AuthenticationException e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Content("AuthenticationException: " + e.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public ActionResult<UserDTO> Register(RegistrationDTO registrationDTO)
        {

            User user = authService.Register(registrationDTO);

            return new UserDTO() { Id = user.Id, Name = user.Name, Email = user.Email };

        }

        /// <summary>
        /// Test for a valid bearer in the request headers
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("TestBearer")]
        public ActionResult<bool> TestBearer()
        {
            return Ok(true);
        }

        /// <summary>
        /// Test if provided bearer JWT is valid
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("TestTokenValidity")]
        public ActionResult<bool> TestTokenValidity(string token)
        {
            return authService.ValidateCurrentToken(token);
        }


    }
}

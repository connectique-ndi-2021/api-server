﻿using contectique_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using static connectique_bo.Entities.Logging.LogEntry;

namespace contectique_api.Controllers
{
    [AllowAnonymous]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {

        private LogService logger;

        public ErrorsController(LogService logger)
        {
            this.logger = logger;
        }



        [Route("error")]
        public JsonResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;


            try
            {
                logger.Log(Guid.NewGuid(), LogType.Error, "Error " + HttpContext.TraceIdentifier, "connectique-api", $"{exception}");
            }
            catch (Exception e)
            {

            }


            Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return new JsonResult(new { errorIdentifier = exception.Message });
        }
    }
}

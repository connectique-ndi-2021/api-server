﻿using connectique_bo.Entities.Identity;
using contectique_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class IdentityController : ControllerBase
    {
        private IdentityService identityService;

        public IdentityController(IdentityService identityService)
        {
            this.identityService = identityService;
        }

        [HttpGet("GetUsers")]
        public ActionResult<List<User>> GetUsers()
        {
            return identityService.GetUsers();
        }

        [HttpGet("GetUser")]
        public ActionResult<User> GetUser(Guid id)
        {
            return identityService.GetUser(id);
        }

        [HttpPost("AddUser")]
        public ActionResult<User> AddUser(User user)
        {
            return identityService.AddUser(user);
        }

        [HttpPut("UpdateUser")]
        public ActionResult<User> UpdateUser(User user)
        {
            return identityService.UpdateUser(user);
        }

        [HttpDelete("DeleteUser")]
        public ActionResult DeleteUser(Guid id)
        {
            identityService.DeleteUser(id);
            return Ok();
        }



    }
}

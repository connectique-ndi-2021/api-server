﻿using connectique_bo.Entities.Business;
using contectique_api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController
    {
        PersonService personService;

        public PersonController(PersonService personService)
        {
            this.personService = personService;
        }

        [HttpGet("GetPersonThatHaveBeenSaved")]
        public ActionResult<List<Person>> GetPersonThatHaveBeenSaved()
        {
            return personService.GetPeopleThatHaveBeenSaved();
        }

        [HttpGet("GetPersonThatSavedSomeone")]
        public ActionResult<List<Person>> GetPersonThatSavedSomeone()
        {
            return personService.GetPersonThatSavedSomeone();
        }
    }
}

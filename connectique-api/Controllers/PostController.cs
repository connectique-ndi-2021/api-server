﻿using System.Net;
using connectique_bo.DTOs.Post;
using connectique_bo.Entities.Business;
using contectique_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostController
    {
        PostService postService;

        public PostController(PostService postService)
        {
            this.postService = postService;
        }

        [HttpGet("GetPostsForShipwreck")]
        public ActionResult<List<Post>> GetPostsForShipwreck([FromQuery] Guid Shipwreck)
        {
            return postService.GetPostsForShipwreck(Shipwreck);
        }

        [HttpPost("PostComment")]
        public ActionResult<bool> PostComment(PostDTO postDTO)
        {
            return postService.PostComment(postDTO);
        }
    }
}

﻿using connectique_bo.Entities.Business;
using contectique_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShipController
    {
        ShipService shipService;

        public ShipController(ShipService shipService)
        {
            this.shipService = shipService; 
        }

        [HttpGet("GetShips")]
        public ActionResult<List<Ship>> GetShips()
        {
            return shipService.GetShips();
        }
    }
}

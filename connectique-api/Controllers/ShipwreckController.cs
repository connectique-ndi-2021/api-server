﻿using connectique_bo.Entities.Business;
using contectique_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShipwreckController
    {
        ShipwreckService shipwreckService;

        public ShipwreckController(ShipwreckService shipwreckService)
        {
            this.shipwreckService = shipwreckService;
        }

        [HttpGet("GetShipwrecksForSavior")]
        public ActionResult<List<Shipwreck>> GetShipwrecksForSavior([FromQuery] Guid savior)
        {
            return shipwreckService.GetShipwrecksForSavior(savior);
        }

        [HttpGet("GetShipwrecksForSavedPeople")]
        public ActionResult<List<Shipwreck>> GetShipwrecksForSavedPeople([FromQuery] Guid savedPerson)
        {
            return shipwreckService.GetShipwrecksForSavedPeople(savedPerson);
        }

        [HttpGet("GetShipwrecksForShip")]
        public ActionResult<List<Shipwreck>> GetShipwrecksForShip([FromQuery] Guid ship)
        {
            return shipwreckService.GetShipwrecksForShip(ship);
        }
    }
}

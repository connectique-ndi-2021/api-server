﻿using contectique_api.Services;
using Microsoft.IO;
using System.Text;
using static connectique_bo.Entities.Logging.LogEntry;

namespace contectique_api.Middlewares
{
    public class RequestResponseLoggingMiddleware
    {//source https://elanderson.net/2019/12/log-requests-and-responses-in-asp-net-core-3/

        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;
        LogService logService;


        public RequestResponseLoggingMiddleware(RequestDelegate next,
                                                ILoggerFactory loggerFactory, LogService logService)
        {
            _next = next;
            _logger = loggerFactory
                      .CreateLogger<RequestResponseLoggingMiddleware>();
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
            this.logService = logService;
        }

        public async Task Invoke(HttpContext context)
        {
            await LogRequest(context);
            await LogResponse(context);
        }

        private async Task LogRequest(HttpContext context)
        {
            context.Request.EnableBuffering();
            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);
            string content = ($"Http Request Information:{Environment.NewLine}" +
                                  $"Protocol:{context.Request.Protocol} " +
                                  $"Schema:{context.Request.Scheme} " +
                                  $"Host: {context.Request.Host} {Environment.NewLine}" +
                                  $"Path: {context.Request.Path} {Environment.NewLine}" +
                                  $"QueryString: {context.Request.Method} {context.Request.QueryString} {Environment.NewLine}" +
                                  $"Request Body: {ReadStreamInChunks(requestStream)} {Environment.NewLine}" +
                                  $"Request Source: {GetIp(context)} {Environment.NewLine}" +
                                  $"Request Headers: {GetHeader(context)} {Environment.NewLine}"
                                  );
            context.Request.Body.Position = 0;


            Guid id = Guid.NewGuid();
            try
            {
                logService.Log(id, LogType.Info, "Request " + context.TraceIdentifier, "connectique-api", content);
            }
            catch (Exception e)
            {

            }
        }

        private string GetHeader(HttpContext context)
        {
            StringBuilder sbHeaders = new StringBuilder();
            foreach (var header in context.Request.Headers)
                sbHeaders.Append($"[{header.Key}: {header.Value}]");

            return sbHeaders.ToString();
        }

        public string GetIp(HttpContext context)
        {
            return context.Connection.RemoteIpAddress.MapToIPv4().ToString() + " "
                + context.Connection.RemoteIpAddress.MapToIPv6().ToString();
        }


        private async Task LogResponse(HttpContext context)
        {
            var originalBodyStream = context.Response.Body;
            await using var responseBody = _recyclableMemoryStreamManager.GetStream();
            context.Response.Body = responseBody;
            await _next(context);
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var text = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            string content = ($"Http Response Information:{Environment.NewLine}" +
                                   $"Schema:{context.Request.Scheme} " +
                                   $"Host: {context.Request.Host} {Environment.NewLine}" +
                                   $"Path: {context.Request.Path} {Environment.NewLine}" +
                                   $"QueryString: {context.Request.QueryString} {Environment.NewLine}" +
                                   $"Response Body: {text}");
            await responseBody.CopyToAsync(originalBodyStream);

            try
            {
                logService.Log(LogType.Info, "Response " + context.TraceIdentifier, "connectique-api", content);
            }
            catch (Exception e)
            {

            }

        }

        private static string ReadStreamInChunks(Stream stream)
        {
            int readsLeft = 5;

            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(readChunk,
                                                   0,
                                                   readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
                readsLeft -= 1;
            } while (readChunkLength > 0 && readsLeft > 0);
            return textWriter.ToString();
        }



    }

    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestResponseLoggingMiddleware>();
        }
    }
}

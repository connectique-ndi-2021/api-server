using connectique_dal.DataAccess;
using connectique_ef;
using connectique_ef.Repositories;
using contectique_api.Middlewares;
using contectique_api.Properties;
using contectique_api.Services;
using contectique_dal.DataAccess;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Connectique API",
        Version = "v1",
        Description = $"Online since {DateTime.Now.ToShortDateString()} {DateTime.Now.ToShortTimeString()}"
    });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please insert JWT with Bearer into field.",
        Name = "Authorization",
        BearerFormat = "JWT",
        Scheme = "Bearer",
        Type = SecuritySchemeType.ApiKey
    });
    c.OperationFilter<AuthResponsesOperationFilter>();

});

string JwtPrivateKey = builder.Configuration["Jwt:PrivateKey"];
string JwtIssuer = builder.Configuration["Jwt:Issuer"];

builder.Services.AddDbContext<DataContext>();
builder.Services.AddTransient<DataContext>();


builder.Services.AddTransient<IIdentityRepository, IdentityRepository>();
builder.Services.AddTransient<IdentityService>();
builder.Services.AddTransient<ILogRepository, LogRepository>();
builder.Services.AddTransient<LogService>();
builder.Services.AddTransient<PersonService>();
builder.Services.AddTransient<ShipwreckService>();
builder.Services.AddTransient<ShipService>();
builder.Services.AddTransient<PostService>();
builder.Services.AddTransient<PersonRepository>();
builder.Services.AddTransient<ShipwreckRepository>();
builder.Services.AddTransient<ShipRepository>();
builder.Services.AddTransient<LinkShipwreckToSavedPeopleRepository>();
builder.Services.AddTransient<LinkShipwreckToSaviorsRepository>();
builder.Services.AddTransient<LinkShipwreckToShipRepository>();
builder.Services.AddTransient<PostRepository>();



builder.Services.AddTransient<AuthService>(option => new AuthService(option.GetRequiredService<IdentityService>(), JwtPrivateKey, JwtIssuer));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(authenticationScheme: JwtBearerDefaults.AuthenticationScheme, configureOptions: options =>
{
    options.IncludeErrorDetails = true;

    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateLifetime = true,

        ValidateIssuerSigningKey = true,
        ValidateIssuer = true,
        ValidateAudience = true,

        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF32.GetBytes(JwtPrivateKey)),
        ValidIssuer = JwtIssuer,
        ValidAudience = JwtIssuer,

    };
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        builder =>
        {
            builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});


var app = builder.Build();

//app.UseRequestResponseLogging();

app.UseHttpsRedirection();

app.UseExceptionHandler("/error");

app.UseCors();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
app.UseSwagger();
app.UseSwaggerUI();
//}

app.Run();



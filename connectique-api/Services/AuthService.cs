﻿using connectique_bo.DTOs.Auth;
using connectique_bo.Entities.Identity;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Authentication;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace contectique_api.Services
{
    public class AuthService
    {

        IdentityService identityService;

        public static readonly string ClaimIdField = "UserId";

        string JwtPrivateKey;
        string JwtIssuer;

        public AuthService(IdentityService identityService, string JwtPrivateKey, string JwtIssuer)
        {
            this.identityService = identityService;

            this.JwtPrivateKey = JwtPrivateKey;
            this.JwtIssuer = JwtIssuer;
        }

        public string Login(AuthenticationDTO authenticationDTO)
        {

            User user = identityService.GetUserByLogin(authenticationDTO.Email);

            if (user is not null)
            {

                byte[] salt = Convert.FromBase64String(user.Salt);

                string hash = HashPassword(authenticationDTO.Password, salt);


                if (hash.Equals(user.Hash))
                {

                    //Login success
                    return GenerateToken(user.Id);

                }

            }

            throw new AuthenticationException("Wrong username or password");

        }

        public User Register(RegistrationDTO registrationDTO)
        {

            byte[] salt = GenerateSalt();
            string hash = HashPassword(registrationDTO.Password, salt);

            User user = new User()
            {
                Id = Guid.NewGuid(),
                Email = registrationDTO.Email,
                Name = registrationDTO.Name,
                Salt = Convert.ToBase64String(salt),
                Hash = hash
            };


            identityService.AddUser(user);

            return user;

        }

        private byte[] GenerateSalt()
        {
            byte[] salt = new byte[128 / 8];
            salt = RandomNumberGenerator.GetBytes(salt.Length);
            return salt;
        }

        private string HashPassword(string password, byte[] salt)
        {
            // generate a 128-bit salt using a cryptographically strong random sequence of nonzero values

            Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
            string hash = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));
            return hash;
        }

        private string GenerateToken(Guid userId)
        {


            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimIdField, userId.ToString()),
                }),
                Expires = DateTime.UtcNow.AddMinutes(10),
                Issuer = JwtIssuer,
                Audience = JwtIssuer,

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF32.GetBytes(JwtPrivateKey)), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public bool ValidateCurrentToken(string token)
        {



            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidIssuer = JwtIssuer,
                    ValidAudience = JwtIssuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF32.GetBytes(JwtPrivateKey)),

                    ValidateLifetime = true,
                    RequireExpirationTime = true

                }, out SecurityToken validatedToken);
            }
            catch
            {
                throw;
            }
            return true;
        }


    }
}

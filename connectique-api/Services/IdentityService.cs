﻿using connectique_bo.DTOs.Auth;
using connectique_bo.Entities.Identity;
using contectique_dal.DataAccess;

namespace contectique_api.Services
{
    public class IdentityService
    {


        private IIdentityRepository identityRepository;

        public IdentityService(IIdentityRepository identityRepository)
        {
            this.identityRepository = identityRepository;
        }


        #region User

        internal List<User> GetUsers()
        {
            return identityRepository.GetUsers();

        }

        internal User GetUser(Guid id)
        {
            return identityRepository.GetUser(id);
        }

        internal User AddUser(User user)
        {
            if (GetUserByLogin(user.Email) is not null)
            {
                throw new Exception("LoginAlreadyTaken");
            }

            identityRepository.AddUser(user);
            return user;
        }

        internal void DeleteUser(Guid id)
        {
            identityRepository.DeleteUser(id);
        }

        internal User UpdateUser(User user)
        {
            identityRepository.UpdateUser(user);
            return user;
        }

        public User GetUserByLogin(string login)
        {
            return identityRepository.GetUserByLogin(login);
        }

        #endregion


    }
}

﻿using connectique_bo.Entities.Logging;
using connectique_dal.DataAccess;
using static connectique_bo.Entities.Logging.LogEntry;

namespace contectique_api.Services
{
    public class LogService
    {
        private ILogRepository logRepository;

        public LogService(ILogRepository logRepository)
        {
            this.logRepository = logRepository;
        }

        public LogEntry Get(Guid id)
        {
            return logRepository.GetLog(id);
        }

        public List<LogEntry> GetList(int pageIndex, int countPerPage)
        {
            return logRepository.GetLogs(pageIndex, countPerPage);
        }

        public void Add(LogEntry value)
        {
            logRepository.Add(value);
        }

        public void Delete(Guid id)
        {
            logRepository.Delete(id);
        }

        public void Update(LogEntry value)
        {
            logRepository.Update(value);
        }

        public void LogException(string title, Exception e)
        {
            Log(LogType.Error, title, e.Source, e.Message + " " + e.StackTrace);
        }

        public void Log(Guid id, LogType type, string title, string source, string content = "")
        {
            logRepository.Add(new LogEntry() { Id = id, Type = type, Date = DateTime.Now, Title = title, Content = content, Source = source });
        }

        public void Log(LogType type, string title, string source, string content = "")
        {
            logRepository.Add(new LogEntry() { Id = Guid.NewGuid(), Type = type, Date = DateTime.Now, Title = title, Content = content, Source = source });
        }


    }
}

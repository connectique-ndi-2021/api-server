﻿using connectique_bo.Entities.Business;
using connectique_bo.Entities.Links;
using connectique_dal.DataAccess;
using connectique_ef.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Services
{
    public class PersonService
    {
        PersonRepository personRepository;
        LinkShipwreckToSavedPeopleRepository linkShipwreckToSavedPeople;
        LinkShipwreckToSaviorsRepository linkShipwreckToSaviorsRepository;

        public PersonService(PersonRepository personRepository, LinkShipwreckToSavedPeopleRepository linkShipwreckToSavedPeople, LinkShipwreckToSaviorsRepository linkShipwreckToSaviorsRepository)
        {
            this.personRepository = personRepository;
            this.linkShipwreckToSavedPeople = linkShipwreckToSavedPeople;
            this.linkShipwreckToSaviorsRepository = linkShipwreckToSaviorsRepository;
        }

        public List<Person> GetPeopleThatHaveBeenSaved()
        {
            List<Person> personThatHaveBeenSaved = new List<Person>();

            List<LinkBetweenShipwreckAndPeopleThatHaveBeenSaved> links = this.linkShipwreckToSavedPeople.Get().ToList();

            foreach(LinkBetweenShipwreckAndPeopleThatHaveBeenSaved l in links)
            {
                Person p = this.personRepository.GetByID(l.PersonSaved);
                if (!personThatHaveBeenSaved.Contains(p))
                    personThatHaveBeenSaved.Add(p);
            }

            return personThatHaveBeenSaved;
        }

        internal ActionResult<List<Person>> GetPersonThatSavedSomeone()
        {
            List<Person> saviors = new List<Person>();

            List<LinkBetweenShipwreckAndSaviors> links = this.linkShipwreckToSaviorsRepository.Get().ToList();

            foreach (LinkBetweenShipwreckAndSaviors l in links)
            {
                Person p = this.personRepository.GetByID(l.Savior);
                if (!saviors.Contains(p))
                    saviors.Add(p);
            }

            return saviors;
        }
    }
}

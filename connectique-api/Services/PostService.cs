﻿using connectique_bo.DTOs.Post;
using connectique_bo.Entities.Business;
using connectique_ef.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Services
{
    public class PostService
    {
        PostRepository postRepository;

        public PostService(PostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public List<Post> GetPostsForShipwreck(Guid shipwreck)
        {
            return this.postRepository.Get(x => x.Shipwreck == shipwreck).ToList();
        }

        internal bool PostComment(PostDTO postDTO)
        {
            Post post = new Post()
            {
                Id = Guid.NewGuid(),
                Author = postDTO.Author,
                Comment = postDTO.Comment,
                Shipwreck = postDTO.Shipwreck,
            };

            this.postRepository.Insert(post);
            return true;
        }
    }
}

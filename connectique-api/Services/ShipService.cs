﻿using connectique_bo.Entities.Business;
using connectique_ef.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Services
{
    public class ShipService
    {
        ShipRepository shipRepository;

        public ShipService(ShipRepository shipRepository)
        {
            this.shipRepository = shipRepository;
        }

        internal ActionResult<List<Ship>> GetShips()
        {
            return this.shipRepository.Get().ToList();
        }
    }
}

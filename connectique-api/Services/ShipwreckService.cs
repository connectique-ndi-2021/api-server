﻿using connectique_bo.Entities.Business;
using connectique_bo.Entities.Links;
using connectique_ef.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace contectique_api.Services
{
    public class ShipwreckService
    {
        ShipwreckRepository shipwreckRepository;
        LinkShipwreckToSavedPeopleRepository linkShipwreckToSavedPeople;
        LinkShipwreckToSaviorsRepository linkShipwreckToSaviorsRepository;
        LinkShipwreckToShipRepository linkShipwreckToShip;

        public ShipwreckService(ShipwreckRepository shipwreckRepository, LinkShipwreckToSavedPeopleRepository linkShipwreckToSavedPeople, LinkShipwreckToSaviorsRepository linkShipwreckToSaviorsRepository, LinkShipwreckToShipRepository linkShipwreckToShip)
        {
            this.shipwreckRepository = shipwreckRepository;
            this.linkShipwreckToSavedPeople = linkShipwreckToSavedPeople;
            this.linkShipwreckToSaviorsRepository = linkShipwreckToSaviorsRepository;
            this.linkShipwreckToShip = linkShipwreckToShip;
        }

        public ActionResult<List<Shipwreck>> GetShipwrecksForSavior(Guid savior)
        {
            List<Shipwreck> shipwrecks = new List<Shipwreck>();

            List<LinkBetweenShipwreckAndSaviors> links = this.linkShipwreckToSaviorsRepository.Get(x => x.Savior == savior).ToList();

            foreach (LinkBetweenShipwreckAndSaviors l in links)
            {
                shipwrecks.Add(this.shipwreckRepository.GetByID(l.Shipwreck));
            }

            return shipwrecks;
        }

        public ActionResult<List<Shipwreck>> GetShipwrecksForSavedPeople(Guid savedPerson)
        {
            List<Shipwreck> shipwrecks = new List<Shipwreck>();

            List<LinkBetweenShipwreckAndPeopleThatHaveBeenSaved> links = this.linkShipwreckToSavedPeople.Get(x => x.PersonSaved == savedPerson).ToList();

            foreach (LinkBetweenShipwreckAndPeopleThatHaveBeenSaved l in links)
            {
                shipwrecks.Add(this.shipwreckRepository.GetByID(l.Shipwreck));
            }

            return shipwrecks;
        }

        public ActionResult<List<Shipwreck>> GetShipwrecksForShip(Guid ship)
        {
            List<Shipwreck> shipwrecks = new List<Shipwreck>();

            List<LinkShipwreckToShip> links = this.linkShipwreckToShip.Get(x => x.Ship == ship).ToList();

            foreach (LinkShipwreckToShip l in links)
            {
                shipwrecks.Add(this.shipwreckRepository.GetByID(l.Shipwreck));
            }

            return shipwrecks;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.DTOs.Post
{
    public class PostDTO
    {
        [MaxLength(200)]
        public string Author { get; set; }

        public string Comment { get; set; }

        public Guid Shipwreck { get; set; }
    }
}

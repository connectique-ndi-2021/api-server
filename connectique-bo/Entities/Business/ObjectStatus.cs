﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.Entities.Business
{
    public enum ObjectStatus
    {
        ToBeApproved,
        Approved,
    }
}

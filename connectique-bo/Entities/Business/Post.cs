﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.Entities.Business
{
    public class Post
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        [MaxLength(200)]
        public string Author { get; set; }

        [Required]
        public string Comment { get; set; }

        [Required]
        public Guid Shipwreck { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.Entities.Business
{
    public class Shipwreck
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public string Description { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public float lat { get; set; }

        [Required]
        public int lng { get; set; }

        [Required]
        public ObjectStatus Status { get; set; }
    }
}

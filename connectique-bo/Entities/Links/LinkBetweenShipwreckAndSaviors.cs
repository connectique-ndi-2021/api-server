﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.Entities.Links
{
    public class LinkBetweenShipwreckAndSaviors
    {
        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required]
        public Guid Shipwreck { get; set; }

        [Required]
        public Guid Savior { get; set; }
    }
}

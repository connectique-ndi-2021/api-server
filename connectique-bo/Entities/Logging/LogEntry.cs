﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_bo.Entities.Logging
{
    public class LogEntry
    {
        [Key]
        public Guid Id { get; set; }

        [MaxLength(500)]
        [Required]
        public string Title { get; set; }

        [MaxLength(200)]
        [Required]
        public string Source { get; set; }

        public string Content { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public LogType Type { get; set; }

        public enum LogType
        {
            Error = 0,
            Warning = 1,
            Info = 2

        }




    }
}

﻿using connectique_bo.Entities.Identity;

namespace contectique_dal.DataAccess
{
    public interface IIdentityRepository
    {
        List<User> GetUsers();
        void AddUser(User user);
        User GetUser(Guid id);
        void DeleteUser(Guid id);
        void UpdateUser(User user);
        User GetUserByLogin(string login);
    }
}

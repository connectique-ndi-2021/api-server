﻿using connectique_bo.Entities.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_dal.DataAccess
{
    public interface ILogRepository
    {
        List<LogEntry> GetLogs(int pageIndex, int countPerPage);

        LogEntry GetLog(Guid id);
        void Update(LogEntry value);
        void Add(LogEntry value);
        void Delete(Guid id);
    }
}

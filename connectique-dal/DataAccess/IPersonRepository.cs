﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Business;

namespace connectique_dal.DataAccess
{
    public interface IPersonRepository
    {
        List<Person> GetSaviors();
    }
}

﻿using connectique_bo.Entities.Business;
using connectique_bo.Entities.Identity;
using connectique_bo.Entities.Links;
using connectique_bo.Entities.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_ef
{
    public class DataContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<LogEntry> LogEntries { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Shipwreck> Shipwrecks { get; set; }
        
        public DbSet<Ship> Ship { get; set; }

        public DbSet<LinkBetweenShipwreckAndPeopleThatHaveBeenSaved> LinkBetweenShipwreckAndPeopleThatHaveBeenSaveds { get; set; }

        public DbSet<LinkBetweenShipwreckAndSaviors> LinkBetweenShipwreckAndSaviors { get; set; }

        public DbSet<LinkShipwreckToShip> LinkShipwreckToShips { get; set; }

        public DbSet<Post> Post { get; set; }


        public string DbPath { get; private set; }
        public MySqlServerVersion DbVersion { get; private set; }

        public DataContext()
        {
            //DbPath = $"Server=127.0.0.1;Port=3306;Database=connectique-db;Uid=root;Pwd=root;";
            //DbPath = $"Server=mysql-bdd.connectique.eu.org;Port=6819;Database=connectique-db;Uid=root;Pwd=15RnMvlnDKnEwRMV;";
            DbPath = $"Server=mysql;Port=3306;Database=connectique-db;Uid=root;Pwd=15RnMvlnDKnEwRMV;";
            DbVersion = new MySqlServerVersion(new Version(5, 7, 28));
        }

        // The following configures EF to create a Sqlite database file in the
        // special "local" folder for your platform.
        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseMySql(DbPath, DbVersion)
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors();
    }
}


/*
 
    PMC Command	                    dotnet CLI command	    Usage

    add-migration <migration name>	Add <migration name>	Creates a migration by adding a migration snapshot.
    Remove-migration	            Remove	                Removes the last migration snapshot.
    Update-database	                Update	                Updates the database schema based on the last migration snapshot.
    Script-migration	            Script	                Generates a SQL script using all the migration snapshots.
 
 
 
 */

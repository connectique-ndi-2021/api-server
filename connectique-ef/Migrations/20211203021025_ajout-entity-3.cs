﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace connectique_ef.Migrations
{
    public partial class ajoutentity3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LinkShipwreckToShips",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Shipwreck = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    Ship = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinkShipwreckToShips", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LinkShipwreckToShips");
        }
    }
}

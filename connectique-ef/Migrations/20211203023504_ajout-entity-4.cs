﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace connectique_ef.Migrations
{
    public partial class ajoutentity4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Guid",
                table: "Persons",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Persons",
                newName: "Guid");
        }
    }
}

﻿using connectique_bo.Entities.Identity;
using contectique_dal.DataAccess;

namespace connectique_ef.Repositories
{
    public class IdentityRepository : IIdentityRepository
    {
        DataContext context;

        public IdentityRepository(DataContext context)
        {
            this.context = context;

        }


        #region User
        public User GetUser(Guid id)
        {
            return context.Users.Single(x => x.Id == id);
        }

        public List<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void DeleteUser(Guid id)
        {
            var user = new User() { Id = id };
            context.Users.Attach(user);
            context.Users.Remove(user);
            context.SaveChanges();
        }

        public void UpdateUser(User user)
        {
            context.Users.Update(user);
            context.SaveChanges();
        }

        public User GetUserByLogin(string login)
        {
            return context.Users.SingleOrDefault(x => x.Email == login);
        }
        #endregion

    }
}

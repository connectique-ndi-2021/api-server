﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Links;

namespace connectique_ef.Repositories
{
    public class LinkShipwreckToSavedPeopleRepository : GenericRepository<LinkBetweenShipwreckAndPeopleThatHaveBeenSaved>
    {
        public LinkShipwreckToSavedPeopleRepository(DataContext context) : base(context) { }
    }
}

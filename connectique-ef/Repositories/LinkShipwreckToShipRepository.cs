﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Links;

namespace connectique_ef.Repositories
{
    public class LinkShipwreckToShipRepository : GenericRepository<LinkShipwreckToShip>
    {
        public LinkShipwreckToShipRepository(DataContext context) : base(context) { }
    }
}

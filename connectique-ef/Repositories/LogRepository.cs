﻿using connectique_bo.Entities.Logging;
using connectique_dal.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace connectique_ef.Repositories
{
    public class LogRepository : ILogRepository
    {
        DataContext context;

        public LogRepository(DataContext context)
        {
            this.context = context;

        }

        public void Add(LogEntry logEntry)
        {
            context.LogEntries.Add(logEntry);
            context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var logEntry = new LogEntry() { Id = id };
            context.LogEntries.Attach(logEntry);
            context.LogEntries.Remove(logEntry);
            context.SaveChanges();
        }

        public LogEntry GetLog(Guid id)
        {
            return context.LogEntries.Single(x => x.Id == id);
        }

        public List<LogEntry> GetLogs(int pageIndex, int countPerPage)
        {
            return context.LogEntries.OrderByDescending(x => x.Date).Skip((pageIndex - 1) * countPerPage).Take(countPerPage).ToList();
        }

        public void Update(LogEntry logEntry)
        {
            context.LogEntries.Update(logEntry);
            context.SaveChanges();
        }
    }
}

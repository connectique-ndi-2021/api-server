﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Business;
using connectique_dal.DataAccess;

namespace connectique_ef.Repositories
{
    public class PersonRepository : GenericRepository<Person>
    {
        public PersonRepository(DataContext context) : base(context) { }
    }
}

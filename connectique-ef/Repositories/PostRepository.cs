﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Business;

namespace connectique_ef.Repositories
{
    public  class PostRepository : GenericRepository<Post>
    {
        public PostRepository(DataContext context) : base(context) { }

        public override void Insert(Post post)
        {
            base.Insert(post);
            context.SaveChanges();
        }
    }
}

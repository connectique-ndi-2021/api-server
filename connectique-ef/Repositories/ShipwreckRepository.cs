﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using connectique_bo.Entities.Business;

namespace connectique_ef.Repositories
{
    public class ShipwreckRepository : GenericRepository<Shipwreck>
    {
        public ShipwreckRepository(DataContext context) : base(context) { }
    }
}
